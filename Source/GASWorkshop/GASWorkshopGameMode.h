// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GASWorkshopGameMode.generated.h"

UCLASS(minimalapi)
class AGASWorkshopGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGASWorkshopGameMode();
};



