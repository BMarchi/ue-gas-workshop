// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "GASWorkshopHUD.generated.h"

UCLASS()
class AGASWorkshopHUD : public AHUD
{
	GENERATED_BODY()

public:
	AGASWorkshopHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

