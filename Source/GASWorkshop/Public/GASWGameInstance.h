// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "GASWGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class GASWORKSHOP_API UGASWGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
};
